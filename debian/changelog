librt-client-rest-perl (1:0.72-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.72.
  * d/copyright: update upstream copyright year.
  * Declare compliance with Debian Policy 4.6.2.

 -- Étienne Mollier <emollier@debian.org>  Thu, 21 Dec 2023 21:05:00 +0100

librt-client-rest-perl (1:0.60-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:34:47 +0100

librt-client-rest-perl (1:0.60-1) unstable; urgency=medium

  * Import upstream version 0.60.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 May 2020 18:09:45 +0200

librt-client-rest-perl (1:0.58-1) unstable; urgency=medium

  * debian/watch: use uscan version 4.
  * Import upstream version 0.58.
  * Update debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 23:12:51 +0200

librt-client-rest-perl (1:0.56-1) unstable; urgency=medium

  * Import upstream version 0.56.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Wed, 26 Dec 2018 17:09:41 +0100

librt-client-rest-perl (1:0.55-1) unstable; urgency=medium

  * Import upstream version 0.55.

 -- gregor herrmann <gregoa@debian.org>  Fri, 14 Dec 2018 19:47:57 +0100

librt-client-rest-perl (1:0.54-1) unstable; urgency=medium

  * Import upstream version 0.54.
  * Update debian/upstream/metadata.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Nov 2018 17:51:40 +0100

librt-client-rest-perl (1:0.52-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Christine Spang from Uploaders. Thanks for your work!
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.52.
  * Update debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
    Also remove info about dropped files and consolidate information.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.
  * Don't install a quasi empty boilerplate manpage.
  * Drop unneeded build dependency on libtest-pod-perl.

 -- gregor herrmann <gregoa@debian.org>  Tue, 10 Apr 2018 18:53:12 +0200

librt-client-rest-perl (1:0.50-1) unstable; urgency=medium

  * Import upstream version 0.50.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Dec 2015 17:49:14 +0100

librt-client-rest-perl (1:0.49-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libcgi-pm-perl.
  * Add debian/upstream/metadata.
  * Update years of packaging copyright.
  * Add build dependency on libhttp-server-simple-perl. Not strictly
    needed during build, since it's embedded in inc/*. But then the tests
    fail during autopkgtest.

 -- gregor herrmann <gregoa@debian.org>  Thu, 18 Jun 2015 00:16:42 +0200

librt-client-rest-perl (1:0.49-1) unstable; urgency=medium

  * New upstream release.
  * Update license for lib/RT/Client/REST.pm.
  * Update years of packaging copyright.
  * Drop build dependency on libtest-pod-coverage-perl.
    Changed to a release test.

 -- gregor herrmann <gregoa@debian.org>  Wed, 14 May 2014 22:48:27 +0200

librt-client-rest-perl (1:0.46-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.46
  * Make Build-Depends-Indep on libwww-perl unversioned.
    Make the Build-Depends-Indep on libwwww-perl unversioned again, as the
    t/80-timeout.t test added an explicit text on the broken 6.04 version
    for LWP::UserAgent.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 26 Apr 2014 22:41:05 +0200

librt-client-rest-perl (1:0.45-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 09 Dec 2013 22:36:01 +0100

librt-client-rest-perl (1:0.44-1) unstable; urgency=low

  * New upstream release.
  * Drop spelling.patch, merged upstream.
  * Update build and runtime dependencies.
  * Update copyright years fod inc/*.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Oct 2013 23:25:28 +0200

librt-client-rest-perl (1:0.43-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Build-depend on libwww-perl >= 6.05. (Closes: #713247)
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Jun 2013 20:20:55 +0200

librt-client-rest-perl (1:0.43-1) unstable; urgency=low

  [ Fabrizio Regalli ]
  * New upstream release 0.41.
  * Bump to 3.9.2 Standard-Version.
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Fixed lintian extended-description-is-probably-too-short message.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release 0.43.
  * debian/copyright: update formatting, add info about newly added
    Module::Install, try to be a bit more specific about the historic
    copyright/license situation.
  * Add /me to Uploaders.
  * Tests: add netbase to B-D-I, unset http_proxy in debian/rules. Enable
    pod{,-coverage} tests.
  * Add patch to fix grammar glitch in the POD.
  * Remove libtest-exception-perl from Depends.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 Jan 2012 21:59:47 +0100

librt-client-rest-perl (1:0.4-1) unstable; urgency=low

  * New upstream release
    - Upstream screwed up version number, add epoch.

 -- Christine Spang <christine@debian.org>  Tue, 23 Mar 2010 21:58:10 -0400

librt-client-rest-perl (0.37-1) unstable; urgency=low

  * Initial Release. (Closes: #542310)

 -- Christine Spang <christine@debian.org>  Tue, 25 Aug 2009 15:25:05 -0400
